//
//  NetworkManager.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

class NetworkManager {
    
    static let shared: NetworkManager = NetworkManager()
    
    func dataPostTask(serviceURL: String, parameters: parametersBody?, completion:@escaping (responseData) -> Void) -> Void {
        requestPostResource(serviceURL: serviceURL, httpMethod: .post, parameters: parameters, completion: completion)
    }
    
    func dataGetTask(serviceURL: String, completion:@escaping (responseData) -> Void) -> Void {
        print(serviceURL)
        requestGetResource(serviceURL: serviceURL, httpMethod: .get, completion: completion)
    }
    
    func dataDeleteTask(serviceURL: String, completion:@escaping (responseData) -> Void) -> Void {
        print(serviceURL)
        requestDeleteResource(serviceURL: serviceURL, httpMethod: .delete, completion: completion)
    }
    
    private func requestPostResource(serviceURL: String, httpMethod: HttpMethod, parameters: parametersBody?, completion: @escaping (responseData) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: URL(string:"\(BaseURL.data)\(serviceURL)")!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.rawValue
        if (parameters != nil) {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
        }
        let sessionTask: URLSessionDataTask = URLSession(configuration: .default).dataTask(with: request) { (data, response, error) in
            if (data != nil) {
                let result = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                completion ((data, result as AnyObject, nil))
            }
            if (error != nil) {
                completion ((nil, nil, error!))
            }
        }
        sessionTask.resume()
    }
    
    private func requestGetResource(serviceURL: String, httpMethod: HttpMethod, completion: @escaping (responseData) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: URL(string:"\(BaseURL.data)\(serviceURL)")!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.rawValue
        let sessionTask: URLSessionDataTask = URLSession(configuration: .default).dataTask(with: request) { (data, response, error) in
            if (data != nil){
                let result = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                completion ((data, result as AnyObject, nil))
            }
            if (error != nil) {
                completion ((nil, nil, error!))
            }
        }
        sessionTask.resume()
    }
    
    private func requestDeleteResource(serviceURL: String, httpMethod: HttpMethod, completion: @escaping (responseData) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: URL(string:"\(BaseURL.data)\(serviceURL)")!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.rawValue
        let sessionTask: URLSessionDataTask = URLSession(configuration: .default).dataTask(with: request) { (data, response, error) in
            if (data != nil){
                let result = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                completion ((data, result as AnyObject, nil))
            }
            if (error != nil) {
                completion ((nil, nil, error!))
            }
        }
        sessionTask.resume()
    }
}
