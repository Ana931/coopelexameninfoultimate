//
//  ProviderNetworkMenuOptions.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import Foundation

class ProviderNetworkMenuOptions {

    static let shared: ProviderNetworkMenuOptions = ProviderNetworkMenuOptions()
    func logout(completion: @escaping (responseData) -> Void) -> Void {
        NetworkMenu.shared.callLogout(serviceURL: "3/authentication/token/session?api_key=\(ApiKey.data)") { (data, response, error) in
            completion((data, response, error))
        }
    }
}
