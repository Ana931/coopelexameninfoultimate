//
//  BaseURL.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

struct BaseURL {
    static let data: String = "https://api.themoviedb.org/"
    static let image: String = "https://image.tmdb.org/t/p/"
}
