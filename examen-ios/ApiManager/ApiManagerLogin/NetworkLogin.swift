//
//  NetworkLogin.swift
//  examen-ios
//
//  Created by User on 28/09/22.
//


import Foundation

class NetworkLogin {
    static let shared: NetworkLogin = NetworkLogin()


    func callLogin(serviceURL: String, parameters: parametersBody?, completion: @escaping (responseData) -> Void) -> Void {
        NetworkManager.shared.dataPostTask(serviceURL: serviceURL, parameters: parameters) { (data, response, error) in
            completion((data, response, error))
        }
    }
    
    func getToken(serviceURL: String, completion: @escaping (responseData) -> Void) -> Void {
        NetworkManager.shared.dataGetTask(serviceURL: serviceURL) { (data, response, error) in
            completion((data, response, error))
        }
    }
    
}
