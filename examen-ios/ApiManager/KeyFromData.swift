//
//  KeyFromData.swift
//  examen-ios
//
//  Created by User on 04/10/22.
//

import Foundation

class KeyFromDataImplement: KeyFromData {
    static var shared: KeyFromData = KeyFromDataImplement()
    var token: String?
}

protocol KeyFromData: class {
    var token: String? { set get }
}
