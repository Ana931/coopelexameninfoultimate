//
//  NetworkListMovie.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

class NetworkListMovie {
    static let shared: NetworkListMovie = NetworkListMovie()

    func callListMovie(serviceURL: String, completion: @escaping (responseData) -> Void) -> Void {
        NetworkManager.shared.dataGetTask(serviceURL: serviceURL) { (data, response, error) in
            completion((data, response, error))
        }
    }
}
