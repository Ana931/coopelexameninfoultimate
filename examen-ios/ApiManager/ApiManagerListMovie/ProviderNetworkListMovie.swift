//
//  ProviderNetworkListMovie.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

class ProviderNetworkListMovie {
    static let shared: ProviderNetworkListMovie = ProviderNetworkListMovie()
    func callListMovie(filterType: ActionType, completion: @escaping (responseData) -> Void) -> Void {
        NetworkListMovie.shared.callListMovie(serviceURL: prepareUrl(filterType: filterType)) { (data, response, error) in
            completion((data, response, error))
        }
    }
    
    private func prepareUrl(filterType: ActionType) -> String {
        var value: String = ""
        switch filterType {
        case .popular:
            value = "popular"
        case .topRate:
            value = "top_rate"
        case .onTv:
            value = "on_tv"
        case .airing:
            value = "airing_today"
        case .none:
            break
        }
        return "3/tv/\(value)?api_key=\(ApiKey.data)&language=en-US&page=1"
    }
}
