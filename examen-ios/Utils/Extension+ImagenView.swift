//
//  Extension+ImagenView.swift
//  examen-ios
//
//  Created by User on 08/11/22.
//

import UIKit

extension UIImageView {
    func setCustomImage(_ imgURLString: String?) {
        guard let imageURLString = imgURLString else {
            self.image = UIImage(named: Images.movieTadeo)
            return
        }
        DispatchQueue.global().async { [weak self] in
            let data = try? Data(contentsOf: URL(string: imageURLString)!)
            DispatchQueue.main.async {
                self?.image = data != nil ? UIImage(data: data!) : UIImage(named: Images.movieTadeo)
            }
        }
    }
}
