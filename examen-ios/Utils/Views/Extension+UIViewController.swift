//
//  Extension+UIViewController.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import UIKit

extension UIViewController {
    
    @objc public func goBack() {
        self.navigationController?.popViewController(animated: true)
    }

    public enum direction {
        case right
        case left
    }
}
