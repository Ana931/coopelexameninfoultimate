//
//  Colors.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

struct Colors {
    static let Blue300: String = "Blue300"
    static let Blue600: String = "Blue600"
    static let BackgroundColor: String = "BackgroundColor"
    static let Red500: String = "Red500"
    static let Gray300: String = "Gray300"
    static let Gray400: String = "Gray400"
}
