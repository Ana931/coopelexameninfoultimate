//
//  Router.swift
//  examen-ios
//
//  Created by User on 04/10/22.
//

import UIKit

class Router {
    private static var navigate: UINavigationController?
    
    init(_ navigation: UINavigationController?) {
        Router.navigate = navigation
    }
    
    static func pushViewController(_ viewController: UIViewController, animated: Bool) {
        Router.navigate?.pushViewController(viewController, animated: animated)
    }
    
    static func presentViewController(_ viewController: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
        Router.navigate?.present(viewController, animated: animated, completion: completion)
    }
    
    static func popViewController(animated: Bool = true) {
        Router.navigate?.popViewController(animated: animated)
    }
}
