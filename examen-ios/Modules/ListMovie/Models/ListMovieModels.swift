//
//  ListMovieModels.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

struct ListMovieModels: Codable {
    let page: Int
    let results: [MovieModels]
    let totalPages: Int
    let totalResults: Int
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case results = "results"
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}
