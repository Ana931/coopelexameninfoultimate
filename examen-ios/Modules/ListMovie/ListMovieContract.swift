//
//  ListMovieContract.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

protocol ListMoviePresenterProtocol: AnyObject {
    var view: ListMoviePresenterToViewProtocol? {get set}
    var interactor: ListMovieInteractorProtocol? {get set}
    var router: ListMovieRouterProtocol? {get set}
    
    func viewDidLoad()
    func callListMovie(actionType: ActionType)
    func navigateToDetailMovie(value: MovieModels)
}

protocol ListMovieInteractorToPresenterProtocol: AnyObject {
    func listMovieSuccess(listMovie: ListMovieModels)
    func listMovieFailure()
}

protocol ListMovieInteractorProtocol: AnyObject {
    var presenter: ListMovieInteractorToPresenterProtocol? {get set}
    func callListMovie(filterType: ActionType)
    
}

protocol ListMoviePresenterToViewProtocol: AnyObject {
    func listMovieSuccess(listMovie: ListMovieModels)
    func listMovieFailure()
}

protocol ListMovieRouterProtocol: AnyObject {
    static func createListMovieViewController() -> ListMovieViewController
    func navigateToDetailMovie(value: MovieModels)
}
