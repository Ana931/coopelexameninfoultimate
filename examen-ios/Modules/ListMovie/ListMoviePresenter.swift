//
//  ListMoviePresenter.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

class ListMoviePresenter {
    var view: ListMoviePresenterToViewProtocol?
    var interactor: ListMovieInteractorProtocol?
    var router: ListMovieRouterProtocol?
}

extension ListMoviePresenter: ListMoviePresenterProtocol {

    func viewDidLoad() {
    }
    
    func callListMovie(actionType: ActionType) {
        interactor?.callListMovie(filterType: actionType)
    }
    
    func navigateToDetailMovie(value: MovieModels) {
        router?.navigateToDetailMovie(value: value)
    }
}

extension ListMoviePresenter: ListMovieInteractorToPresenterProtocol {
    func listMovieSuccess(listMovie: ListMovieModels) {
        view?.listMovieSuccess(listMovie: listMovie)
    }
    
    func listMovieFailure() {
        view?.listMovieFailure()
    }
}

