//
//  DetailMovieViewController.swift
//  examen-ios
//
//  Created by User on 01/11/22.
//

import UIKit

class DetailMovieViewController: BaseViewController {
    var presenter: DetailMoviePresenterProtocol?
    private var scrollView: UIScrollView = {
        let scrollView: UIScrollView = UIScrollView()
        return scrollView
    }()
    private lazy var pictureMovie: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.image = UIImage(named: Images.movieTadeo)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    private var stackView: UIStackView = {
        let stackView: UIStackView = UIStackView()
        stackView.distribution = .equalSpacing
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 8
        return stackView
    }()
    private lazy var titleMovieTagLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        label.text = "Titulo de la pelicula:"
        label.textColor = UIColor.gray
        return label
    }()
    private lazy var titleMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        return label
    }()
    private lazy var dateMovieTagLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        label.text = "Fecha:"
        label.textColor = UIColor.gray
        return label
    }()
    private lazy var dateMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        return label
    }()
    private lazy var voteMovieMovieTagLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        label.text = "Votacion:"
        label.textColor = UIColor.gray
        return label
    }()
    private lazy var voteMovieMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        return label
    }()
    private lazy var pointMovieMovieTagLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        label.text = "Puntaje:"
        label.textColor = UIColor.gray
        return label
    }()
    private lazy var pointMovieMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        return label
    }()
    private lazy var descriptionShortMovieTagLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        label.text = "Descripcion corta:"
        label.textColor = UIColor.gray
        return label
    }()
    private lazy var descriptionShortMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.white
        label.numberOfLines = .zero
        return label
    }()
    private lazy var addFavoriteButton: UIButton = {
        let button: UIButton = UIButton()
        button.setTitle("Agregar a favoritos", for: .normal)
        return button
    }()
    private var addFavoriteImagen: UIImageView = {
        let imagenView: UIImageView = UIImageView()
        imagenView.image = UIImage(named: Images.favoriteOffIcon)
        return imagenView
    }()
    
    private enum LayoutConstant {
        static let spacing: CGFloat = 8.0
        static let itemHeight: CGFloat = 250.0
    }
    private var value: MovieModels?
    
    required init(value: MovieModels) {
        super.init(nibName: nil, bundle: nil)
        self.value = value
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        commontInit()
        guard let value: MovieModels = self.value else {
            return
        }
        dataContent(with: value)
    }
    
    private func commontInit() {
        addSubViews()
        addConstraints()
        addTargets()
        view.backgroundColor = UIColor.init(named: Colors.Gray400)
    }
    
    private func addSubViews() {
        self.view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        stackView.addArrangedSubview(pictureMovie)
        stackView.addArrangedSubview(titleMovieTagLabel)
        stackView.addArrangedSubview(titleMovieLabel)
        stackView.addArrangedSubview(dateMovieTagLabel)
        stackView.addArrangedSubview(dateMovieLabel)
        stackView.addArrangedSubview(voteMovieMovieTagLabel)
        stackView.addArrangedSubview(voteMovieMovieLabel)
        stackView.addArrangedSubview(pointMovieMovieTagLabel)
        stackView.addArrangedSubview(pointMovieMovieLabel)
        stackView.addArrangedSubview(descriptionShortMovieTagLabel)
        stackView.addArrangedSubview(descriptionShortMovieLabel)
        scrollView.addSubview(addFavoriteButton)
        scrollView.addSubview(addFavoriteImagen)
    }
    
    private func addConstraints() {
        addConstraintForScrollView()
        addConstraintsForPictureMovie()
        addConstraintForStackView()
        addConstraintForTitleMovieTagLabel()
        addConstraintForTitleMovieLabel()
        addConstraintForDateMovieTagLabel()
        addConstraintForDateMovieLabel()
        addConstraintForVoteMovieMovieTagLabel()
        addConstraintForVoteMovieMovieLabel()
        addConstraintForPointMovieMovieTagLabel()
        addConstraintForPointMovieMovieLabel()
        addConstraintForDescriptionShortMovieTagLabel()
        addConstraintForDescriptionShortMovieLabel()
        addContraintForAddMoreButton()
        addContraintForAddFavoriteImagen()
    }
    
    private func addConstraintForScrollView() {
        scrollView.setAnchors(topAnchor: self.view.safeAreaLayoutGuide.topAnchor, bottomAnchor: self.view.safeAreaLayoutGuide.bottomAnchor, leadingAnchor: self.view.safeAreaLayoutGuide.leadingAnchor, trailingAnchor: self.view.safeAreaLayoutGuide.trailingAnchor)
    }
    
    private func addConstraintsForPictureMovie() {
        pictureMovie.setAnchors(topAnchor: view.topAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor, topConstant: 50, leadingConstant: -5, trailingConstant: 5)
        pictureMovie.setHeight(heightConstant: 50)
    }
    
    private func addConstraintForStackView() {
        stackView.setAnchors(topAnchor: pictureMovie.bottomAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor, topConstant: 60)
    }
    
    private func addConstraintForTitleMovieTagLabel() {
        titleMovieTagLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        titleMovieTagLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addConstraintForTitleMovieLabel() {
        titleMovieLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        titleMovieLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addConstraintForDateMovieTagLabel() {
        dateMovieTagLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        dateMovieTagLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addConstraintForDateMovieLabel() {
        dateMovieLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        dateMovieLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addConstraintForVoteMovieMovieTagLabel() {
        voteMovieMovieTagLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        voteMovieMovieTagLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addConstraintForVoteMovieMovieLabel() {
        voteMovieMovieLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        voteMovieMovieLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addConstraintForPointMovieMovieTagLabel() {
        pointMovieMovieTagLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        pointMovieMovieTagLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addConstraintForPointMovieMovieLabel() {
        pointMovieMovieLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        pointMovieMovieLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addConstraintForDescriptionShortMovieTagLabel() {
        descriptionShortMovieTagLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        descriptionShortMovieTagLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addConstraintForDescriptionShortMovieLabel() {
        descriptionShortMovieLabel.setLeading(leadingAnchor: view.leadingAnchor, leadingConstant: 20)
        descriptionShortMovieLabel.setTrailing(trailingAnchor: view.trailingAnchor, trailingConstant: -20)
    }
    
    private func addContraintForAddMoreButton() {
        addFavoriteButton.setAnchors(topAnchor: stackView.bottomAnchor, leadingAnchor: view.leadingAnchor)
        addFavoriteButton.setWidth(widthConstant: 180)
    }
    
    private func addContraintForAddFavoriteImagen() {
        addFavoriteImagen.setAnchors(topAnchor: stackView.bottomAnchor, leadingAnchor: addFavoriteButton.trailingAnchor, trailingAnchor: view.trailingAnchor, leadingConstant: 20)
        addFavoriteImagen.setHeight(heightConstant: 40)
        addFavoriteImagen.setWidth(widthConstant: 40)
    }
    
    func dataContent(with dataContent: MovieModels) {
        titleMovieLabel.text = dataContent.name
        dateMovieLabel.text = dataContent.firstAirDate
        voteMovieMovieLabel.text = "\(dataContent.voteAverage ?? 0.0)"
        pointMovieMovieLabel.text = "\(dataContent.voteCount)"
        descriptionShortMovieLabel.text = dataContent.overview
        pictureMovie.setCustomImage(BaseURL.image+"w780"+(dataContent.backdropPath ?? ""))
    }
    
    private func addTargets() {
        addFavoriteButton.addTarget(self, action: #selector(addFavoriteAction), for: .touchDown)
    }
    
    @objc private func addFavoriteAction() {
        guard let value: MovieModels = self.value else {
            return
        }
        presenter?.addingFavoriteMovie(movie: value)
    }
}

extension DetailMovieViewController: DetailMoviePresenterToViewProtocol {
    
    func addingFavoriteMovieSuccess() {
        addFavoriteImagen.image = UIImage(named: Images.favoriteOnIcon)
    }
    
    func addingFavoriteMovieFailure() {
        showAlert(message: "Error al agregar en favoritos")
    }
}
