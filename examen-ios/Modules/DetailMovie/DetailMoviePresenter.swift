//
//  DetailMoviePresenter.swift
//  examen-ios
//
//  Created by User on 01/11/22.
//

import Foundation

class DetailMoviePresenter {
    var view: DetailMoviePresenterToViewProtocol?
    var interactor: DetailMovieInteractorProtocol?
    var router: DetailMovieRouterProtocol?
}

extension DetailMoviePresenter: DetailMoviePresenterProtocol {
    func viewDidLoad() {
    }
    
    func addingFavoriteMovie(movie: MovieModels) {
        interactor?.addingFavoriteMovie(movie: movie)
    }
}

extension DetailMoviePresenter: DetailMovieInteractorToPresenterProtocol {
    func addingFavoriteMovieSuccess() {
        view?.addingFavoriteMovieSuccess()
    }
    
    func addingFavoriteMovieFailure() {
        view?.addingFavoriteMovieFailure()
    }
}
