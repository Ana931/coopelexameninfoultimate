//
//  DetailMovieInteractor.swift
//  examen-ios
//
//  Created by User on 01/11/22.
//

import Foundation

class DetailMovieInteractor: DetailMovieInteractorProtocol {
    var presenter: DetailMovieInteractorToPresenterProtocol?
    var isAddFavoriteMovie: Bool = false
    
    func addingFavoriteMovie(movie: MovieModels) {
        if !isAddFavoriteMovie {
            MoviesFavoriteFrom.array.append(movie)
            isAddFavoriteMovie = true
            presenter?.addingFavoriteMovieSuccess()
        }
    }
}
