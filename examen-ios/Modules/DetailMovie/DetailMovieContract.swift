//
//  DetailMovieContract.swift
//  examen-ios
//
//  Created by User on 01/11/22.
//

import Foundation

protocol DetailMoviePresenterProtocol: AnyObject {
    var view: DetailMoviePresenterToViewProtocol? {get set}
    var interactor: DetailMovieInteractorProtocol? {get set}
    var router: DetailMovieRouterProtocol? {get set}
    
    func viewDidLoad()
    func addingFavoriteMovie(movie: MovieModels)
}

protocol DetailMovieInteractorToPresenterProtocol: AnyObject {
    func addingFavoriteMovieSuccess()
    func addingFavoriteMovieFailure()
}

protocol DetailMovieInteractorProtocol: AnyObject {
    var presenter: DetailMovieInteractorToPresenterProtocol? {get set}
    func addingFavoriteMovie(movie: MovieModels)
}

protocol DetailMoviePresenterToViewProtocol: AnyObject {
    init(value: MovieModels)
    func addingFavoriteMovieSuccess()
    func addingFavoriteMovieFailure()
}

protocol DetailMovieRouterProtocol: AnyObject {
    static func createDetailMovieViewController(value: MovieModels) -> DetailMovieViewController
}
