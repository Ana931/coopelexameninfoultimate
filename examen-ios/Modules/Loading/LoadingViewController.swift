//
//  LoadingViewController.swift
//  examen-ios
//
//  Created by User on 03/10/22.
//

import UIKit
import Lottie

class LoadingViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
    }
    
    private func commonInit() {
        addSubViews()
        addConstraints()
    }
    
    private func addSubViews() {
        
    }
    
    private func addConstraints() {
        
    }
}
