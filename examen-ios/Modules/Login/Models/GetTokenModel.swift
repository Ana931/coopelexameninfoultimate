//
//  GetTokenModel.swift
//  examen-ios
//
//  Created by User on 04/10/22.
//

import Foundation

struct GetTokenModel: Codable {
    let success: Bool?
    let expiresAt: String?
    let requestToken: String?
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case expiresAt = "expires_at"
        case requestToken = "request_token"
    }
}
