//
//  ProfileRouter.swift
//  examen-ios
//
//  Created by User on 24/10/22.
//

import Foundation

class ProfileRouter: ProfileRouterProtocol {
    
    static func createProfileViewController() -> ProfileViewController {
        let profileViewController: ProfileViewController = ProfileViewController()
        let presenter: ProfilePresenterProtocol & ProfileInteractorToPresenterProtocol = ProfilePresenter()
        let interactor: ProfileInteractorProtocol = ProfileInteractor()
        let router: ProfileRouterProtocol = ProfileRouter()
        profileViewController.presenter = presenter
        presenter.view = profileViewController
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return profileViewController
    }
    
    func closeMenu() {
        Router.popViewController(animated: true)
    }
}
