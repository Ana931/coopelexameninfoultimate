//
//  ProfileContract.swift
//  examen-ios
//
//  Created by User on 24/10/22.
//

import Foundation

protocol ProfilePresenterProtocol: AnyObject {
    var view: ProfilePresenterToViewProtocol? {get set}
    var interactor: ProfileInteractorProtocol? {get set}
    var router: ProfileRouterProtocol? {get set}
    
    func viewDidLoad()
    func closeMenu()
}

protocol ProfileInteractorToPresenterProtocol: AnyObject {
    func listMovieSuccess(listMovie: ListMovieModels)
    func listMovieFailure()
}

protocol ProfileInteractorProtocol: AnyObject {
    var presenter: ProfileInteractorToPresenterProtocol? {get set}
    func callListMovie()
    
}

protocol ProfilePresenterToViewProtocol: AnyObject {
    func listMovieSuccess(listMovie: ListMovieModels)
    func listMovieFailure()
}

protocol ProfileRouterProtocol: AnyObject {
    static func createProfileViewController() -> ProfileViewController
    func closeMenu()
}
